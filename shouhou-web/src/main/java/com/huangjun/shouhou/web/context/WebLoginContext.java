package com.huangjun.shouhou.web.context;

import com.huangjun.common.context.LoginContext;
import org.springframework.stereotype.Component;

/**
 * web登录用户上下文
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Component
public class WebLoginContext implements LoginContext {

    /**
     * 获取当前操作员id
     *
     * @return
     */
    @Override
    public String getCurrentUser() {
        return "admin";
    }

}

