package com.huangjun.shouhou.web.api.order;

import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.order.OrderAddDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderUpdateDTO;
import com.huangjun.shouhou.pojo.qo.order.OrderQO;
import com.huangjun.shouhou.pojo.vo.order.OrderListVO;
import com.huangjun.shouhou.pojo.vo.order.OrderShowVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 【工单中心】API
 * <p>swagger接口文档
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Api(tags = "【工单中心】API")
public interface OrderAPI {

    /**
     * 新增【工单中心】
     */
    @ApiOperation(value = "新增【工单中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderAddDTO", dataTypeClass = OrderAddDTO.class, value = "新增【工单中心】参数", paramType = "body"),
    })
    ResponseEntity<OrderShowVO> save(OrderAddDTO orderAddDTO) throws Exception;

    /**
     * 修改【工单中心】
     */
    @ApiOperation(value = "修改【工单中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderUpdateDTO", dataTypeClass = OrderUpdateDTO.class, value = "修改【工单中心】参数", paramType = "body"),
    })
    ResponseEntity<OrderShowVO> update(OrderUpdateDTO orderUpdateDTO);

    /**
     * 分页查询【工单中心】
     */
    @ApiOperation(value = "分页查询【工单中心】")
    ResponseEntity<PageVO<OrderListVO>> list(OrderQO orderQO);

    /**
     * 查看【工单中心】详情
     */
    @ApiOperation(value = "查看【工单中心】详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", dataTypeClass = Long.class, value = "【工单中心】id", paramType = "path"),
    })
    ResponseEntity<OrderShowVO> show(Long orderId);

    /**
     * 删除单个【工单中心】
     */
    @ApiOperation(value = "删除单个【工单中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", dataTypeClass = Long.class, value = "【工单中心】id", paramType = "path"),
    })
    ResponseEntity<Integer> delete(Long orderId);

    /**
     * 批量删除【工单中心】
     */
    @ApiOperation(value = "批量删除【工单中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataTypeClass = Long.class, allowMultiple = true, value = "id数组", paramType = "body"),
    })
    ResponseEntity<Integer> deleteBatch(Long[] id);

    /**
     * 导出【工单中心】excel
     */
    @ApiOperation(value = "导出【工单中心】excel")
    void exportExcel(OrderQO orderQO, HttpServletResponse response) throws Exception;

    /**
     * 导入【工单中心】excel
     */
    @ApiOperation(value = "导入【工单中心】excel")
    ResponseEntity<Integer> importExcel(MultipartFile file) throws Exception;

    /**
     * 下载【工单中心】excel模板
     */
    @ApiOperation(value = "下载【工单中心】excel模板")
    void downloadExcelTemplate(HttpServletResponse response) throws Exception;

}

