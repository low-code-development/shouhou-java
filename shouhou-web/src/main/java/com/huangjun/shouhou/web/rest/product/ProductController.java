package com.huangjun.shouhou.web.rest.product;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.common.util.DateUtil;
import com.huangjun.shouhou.constant.ProductType;
import com.huangjun.shouhou.excel.handler.ConstConstraintHandler;
import com.huangjun.shouhou.excel.handler.TemplateCellStyleStrategy;
import com.huangjun.shouhou.excel.handler.TitleDescriptionWriteHandler;
import com.huangjun.shouhou.excel.listener.SyncReadExcelListener;
import com.huangjun.shouhou.pojo.dto.product.ProductAddDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductExcelDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.product.ProductMapper;
import com.huangjun.shouhou.pojo.po.product.ProductPO;
import com.huangjun.shouhou.pojo.qo.product.ProductQO;
import com.huangjun.shouhou.pojo.vo.product.ProductExcelVO;
import com.huangjun.shouhou.pojo.vo.product.ProductListVO;
import com.huangjun.shouhou.pojo.vo.product.ProductShowVO;
import com.huangjun.shouhou.service.product.ProductService;
import com.huangjun.shouhou.web.AbstractController;
import com.huangjun.shouhou.web.api.product.ProductAPI;
import com.huangjun.shouhou.web.constant.WebConst;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 【产品管理】控制器
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@RestController
@RequestMapping(WebConst.ModulePath.PRODUCT + "/product")
public class ProductController extends AbstractController implements ProductAPI {

    @Autowired
    private ProductService productService;
    @Autowired
    private Validator validator;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ProductShowVO> save(@Valid @RequestBody ProductAddDTO productAddDTO) throws Exception {
        ProductPO product = productService.save(productAddDTO);
        return ResponseEntity.created(new URI(WebConst.ModulePath.PRODUCT + "/product/" + product.getProductId()))
                .body(ProductMapper.INSTANCE.toShowVO(product));
    }

    @Override
    @PutMapping
    public ResponseEntity<ProductShowVO> update(@Valid @RequestBody ProductUpdateDTO productUpdateDTO) {
        ProductPO product = productService.update(productUpdateDTO);
        return ResponseEntity.ok(ProductMapper.INSTANCE.toShowVO(product));
    }

    @Override
    @GetMapping
    public ResponseEntity<PageVO<ProductListVO>> list(@Valid ProductQO productQO) {
        PageVO<ProductListVO> page = productService.list(productQO);
        return ResponseEntity.ok(page);
    }

    @Override
    @GetMapping(value = "/options")
    public ResponseEntity<List<OptionVO<Long, String>>> findOptions(OptionQO<Long, String> qo) {
        List<OptionVO<Long, String>> options = productService.findOptions(qo);
        return ResponseEntity.ok(options);
    }

    @Override
    @GetMapping(value = "/{productId}")
    public ResponseEntity<ProductShowVO> show(@PathVariable Long productId) {
        ProductShowVO productShowVO = productService.show(productId);
        return ResponseEntity.ok(productShowVO);
    }

    @Override
    @DeleteMapping(value = "/{productId}")
    public ResponseEntity<Integer> delete(@PathVariable Long productId) {
        int count = productService.delete(productId);
        return ResponseEntity.ok(count);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<Integer> deleteBatch(@RequestBody Long[] id) {
        if (ArrayUtils.isEmpty(id)) {
            throw new BusinessException(ErrorCode.PARAM_IS_NULL);
        }
        int count = productService.delete(id);
        return ResponseEntity.ok(count);
    }

    @Override
    @GetMapping("/export")
    public void exportExcel(@Valid ProductQO productQO, HttpServletResponse response) throws Exception {
        productQO.setPageSize(Integer.MAX_VALUE);
        productQO.setPageNo(1);
        List<ProductListVO> list = productService.list(productQO).getList();
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("产品管理导出", "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), ProductExcelVO.class)
                .sheet()
                .doWrite(ProductMapper.INSTANCE.toExcelVOList(list));
    }

    @Override
    @PostMapping("/import")
    public ResponseEntity<Integer> importExcel(@RequestPart MultipartFile file) throws Exception {
        SyncReadExcelListener<ProductExcelDTO> excelListener = new SyncReadExcelListener();
        EasyExcel.read(file.getInputStream())
                .head(ProductExcelDTO.class)
                .sheet()
                .headRowNumber(3)
                .registerReadListener(excelListener)
                .doRead();
        List<ProductAddDTO> list = excelListener.getList().stream()
                .map(excelDTO -> {
                    ProductAddDTO addDTO = ProductMapper.INSTANCE.fromExcelDTO(excelDTO);
                    // 校验数据
                    Set<ConstraintViolation<ProductAddDTO>> set = validator.validate(addDTO);
                    if (!set.isEmpty()) {
                        ConstraintViolation<ProductAddDTO> violation = set.stream().findFirst().get();
                        String errorMsg = "第" + (excelDTO.getRowIndex() + 1) + "行数据不合法：" + violation.getMessage();
                        throw new ConstraintViolationException(errorMsg, set);
                    }
                    return addDTO;
                })
                .collect(Collectors.toList());
        int count = productService.batchSave(list);
        return ResponseEntity.ok(count);
    }

    @Override
    @GetMapping("/template")
    public void downloadExcelTemplate(HttpServletResponse response) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String title = "产品管理导入模板(" + DateUtil.getDateStr(new Date()) + ")";
        String fileName = URLEncoder.encode(title, "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        String[] description = new String[]{
                "模版前三行标题请勿修改",
                "带“*”号为必填项",
        };
        String[] productTypeConstraint = Arrays.stream(ProductType.values()).map(ProductType::getDesc).toArray(String[]::new);
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream())
                .registerWriteHandler(new ConstConstraintHandler(productTypeConstraint, 3, 3, 2, 2))
                // 第一行是标题，第二行是说明
                .registerWriteHandler(new TitleDescriptionWriteHandler(title, description, ProductExcelDTO.class))
                // 自定义模板单元格样式
                .registerWriteHandler(new TemplateCellStyleStrategy())
                .build();
        WriteSheet writeSheet = EasyExcel.writerSheet(0, "Sheet1")
                .head(ProductExcelDTO.class)
                // 从第三行开始写表头
                .relativeHeadRowIndex(2)
                .build();
        excelWriter.write(Arrays.asList(ProductExcelDTO.example()), writeSheet);

        excelWriter.finish();
    }

}


