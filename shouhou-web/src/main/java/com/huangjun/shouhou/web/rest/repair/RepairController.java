package com.huangjun.shouhou.web.rest.repair;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.common.util.DateUtil;
import com.huangjun.shouhou.constant.RepairStatus;
import com.huangjun.shouhou.constant.RepairType;
import com.huangjun.shouhou.excel.handler.ConstConstraintHandler;
import com.huangjun.shouhou.excel.handler.TemplateCellStyleStrategy;
import com.huangjun.shouhou.excel.handler.TitleDescriptionWriteHandler;
import com.huangjun.shouhou.excel.listener.SyncReadExcelListener;
import com.huangjun.shouhou.pojo.dto.repair.RepairAddDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairExcelDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.repair.RepairMapper;
import com.huangjun.shouhou.pojo.po.repair.RepairPO;
import com.huangjun.shouhou.pojo.qo.repair.RepairQO;
import com.huangjun.shouhou.pojo.vo.repair.RepairExcelVO;
import com.huangjun.shouhou.pojo.vo.repair.RepairListVO;
import com.huangjun.shouhou.pojo.vo.repair.RepairShowVO;
import com.huangjun.shouhou.service.repair.RepairService;
import com.huangjun.shouhou.web.AbstractController;
import com.huangjun.shouhou.web.api.repair.RepairAPI;
import com.huangjun.shouhou.web.constant.WebConst;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 【报修中心】控制器
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@RestController
@RequestMapping(WebConst.ModulePath.REPAIR + "/repair")
public class RepairController extends AbstractController implements RepairAPI {

    @Autowired
    private RepairService repairService;
    @Autowired
    private Validator validator;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<RepairShowVO> save(@Valid @RequestBody RepairAddDTO repairAddDTO) throws Exception {
        RepairPO repair = repairService.save(repairAddDTO);
        return ResponseEntity.created(new URI(WebConst.ModulePath.REPAIR + "/repair/" + repair.getRepairId()))
                .body(RepairMapper.INSTANCE.toShowVO(repair));
    }

    @Override
    @PutMapping
    public ResponseEntity<RepairShowVO> update(@Valid @RequestBody RepairUpdateDTO repairUpdateDTO) {
        RepairPO repair = repairService.update(repairUpdateDTO);
        return ResponseEntity.ok(RepairMapper.INSTANCE.toShowVO(repair));
    }

    @Override
    @GetMapping
    public ResponseEntity<PageVO<RepairListVO>> list(@Valid RepairQO repairQO) {
        PageVO<RepairListVO> page = repairService.list(repairQO);
        return ResponseEntity.ok(page);
    }

    @Override
    @GetMapping(value = "/{repairId}")
    public ResponseEntity<RepairShowVO> show(@PathVariable Long repairId) {
        RepairShowVO repairShowVO = repairService.show(repairId);
        return ResponseEntity.ok(repairShowVO);
    }

    @Override
    @DeleteMapping(value = "/{repairId}")
    public ResponseEntity<Integer> delete(@PathVariable Long repairId) {
        int count = repairService.delete(repairId);
        return ResponseEntity.ok(count);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<Integer> deleteBatch(@RequestBody Long[] id) {
        if (ArrayUtils.isEmpty(id)) {
            throw new BusinessException(ErrorCode.PARAM_IS_NULL);
        }
        int count = repairService.delete(id);
        return ResponseEntity.ok(count);
    }

    @Override
    @GetMapping("/export")
    public void exportExcel(@Valid RepairQO repairQO, HttpServletResponse response) throws Exception {
        repairQO.setPageSize(Integer.MAX_VALUE);
        repairQO.setPageNo(1);
        List<RepairListVO> list = repairService.list(repairQO).getList();
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("报修中心导出", "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), RepairExcelVO.class)
                .sheet()
                .doWrite(RepairMapper.INSTANCE.toExcelVOList(list));
    }

    @Override
    @PostMapping("/import")
    public ResponseEntity<Integer> importExcel(@RequestPart MultipartFile file) throws Exception {
        SyncReadExcelListener<RepairExcelDTO> excelListener = new SyncReadExcelListener();
        EasyExcel.read(file.getInputStream())
                .head(RepairExcelDTO.class)
                .sheet()
                .headRowNumber(3)
                .registerReadListener(excelListener)
                .doRead();
        List<RepairAddDTO> list = excelListener.getList().stream()
                .map(excelDTO -> {
                    RepairAddDTO addDTO = RepairMapper.INSTANCE.fromExcelDTO(excelDTO);
                    // 校验数据
                    Set<ConstraintViolation<RepairAddDTO>> set = validator.validate(addDTO);
                    if (!set.isEmpty()) {
                        ConstraintViolation<RepairAddDTO> violation = set.stream().findFirst().get();
                        String errorMsg = "第" + (excelDTO.getRowIndex() + 1) + "行数据不合法：" + violation.getMessage();
                        throw new ConstraintViolationException(errorMsg, set);
                    }
                    return addDTO;
                })
                .collect(Collectors.toList());
        int count = repairService.batchSave(list);
        return ResponseEntity.ok(count);
    }

    @Override
    @GetMapping("/template")
    public void downloadExcelTemplate(HttpServletResponse response) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String title = "报修中心导入模板(" + DateUtil.getDateStr(new Date()) + ")";
        String fileName = URLEncoder.encode(title, "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        String[] description = new String[]{
                "模版前三行标题请勿修改",
                "带“*”号为必填项",
                "“客户”、“客户产品”、“选择维修工”请填入id值",
        };
        String[] repairTypeConstraint = Arrays.stream(RepairType.values()).map(RepairType::getDesc).toArray(String[]::new);
        String[] repairStatusConstraint = Arrays.stream(RepairStatus.values()).map(RepairStatus::getDesc).toArray(String[]::new);
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream())
                .registerWriteHandler(new ConstConstraintHandler(repairTypeConstraint, 3, 3, 5, 5))
                .registerWriteHandler(new ConstConstraintHandler(repairStatusConstraint, 3, 3, 7, 7))
                // 第一行是标题，第二行是说明
                .registerWriteHandler(new TitleDescriptionWriteHandler(title, description, RepairExcelDTO.class))
                // 自定义模板单元格样式
                .registerWriteHandler(new TemplateCellStyleStrategy())
                .build();
        WriteSheet writeSheet = EasyExcel.writerSheet(0, "Sheet1")
                .head(RepairExcelDTO.class)
                // 从第三行开始写表头
                .relativeHeadRowIndex(2)
                .build();
        excelWriter.write(Arrays.asList(RepairExcelDTO.example()), writeSheet);

        excelWriter.finish();
    }

}


