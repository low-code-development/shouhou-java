package com.huangjun.shouhou.web.api.user;

import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.user.UserAddDTO;
import com.huangjun.shouhou.pojo.dto.user.UserUpdateDTO;
import com.huangjun.shouhou.pojo.qo.user.UserQO;
import com.huangjun.shouhou.pojo.vo.user.UserListVO;
import com.huangjun.shouhou.pojo.vo.user.UserShowVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * 【用户表(员工表)】API
 * <p>swagger接口文档
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Api(tags = "【用户表(员工表)】API")
public interface UserAPI {

    /**
     * 新增【用户表(员工表)】
     */
    @ApiOperation(value = "新增【用户表(员工表)】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAddDTO", dataTypeClass = UserAddDTO.class, value = "新增【用户表(员工表)】参数", paramType = "body"),
    })
    ResponseEntity<UserShowVO> save(UserAddDTO userAddDTO) throws Exception;

    /**
     * 修改【用户表(员工表)】
     */
    @ApiOperation(value = "修改【用户表(员工表)】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userUpdateDTO", dataTypeClass = UserUpdateDTO.class, value = "修改【用户表(员工表)】参数", paramType = "body"),
    })
    ResponseEntity<UserShowVO> update(UserUpdateDTO userUpdateDTO);

    /**
     * 分页查询【用户表(员工表)】
     */
    @ApiOperation(value = "分页查询【用户表(员工表)】")
    ResponseEntity<PageVO<UserListVO>> list(UserQO userQO);

    /**
     * 查询【用户表(员工表)】选项列表
     */
    @ApiOperation(value = "查询【用户表(员工表)】选项列表")
    ResponseEntity<List<OptionVO<Long, String>>> findOptions(OptionQO<Long, String> qo);

    /**
     * 查看【用户表(员工表)】详情
     */
    @ApiOperation(value = "查看【用户表(员工表)】详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", dataTypeClass = Long.class, value = "【用户表(员工表)】id", paramType = "path"),
    })
    ResponseEntity<UserShowVO> show(Long userId);

    /**
     * 删除单个【用户表(员工表)】
     */
    @ApiOperation(value = "删除单个【用户表(员工表)】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", dataTypeClass = Long.class, value = "【用户表(员工表)】id", paramType = "path"),
    })
    ResponseEntity<Integer> delete(Long userId);

    /**
     * 批量删除【用户表(员工表)】
     */
    @ApiOperation(value = "批量删除【用户表(员工表)】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataTypeClass = Long.class, allowMultiple = true, value = "id数组", paramType = "body"),
    })
    ResponseEntity<Integer> deleteBatch(Long[] id);

}

