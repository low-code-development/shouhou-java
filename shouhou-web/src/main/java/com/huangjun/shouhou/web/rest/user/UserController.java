package com.huangjun.shouhou.web.rest.user;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.user.UserAddDTO;
import com.huangjun.shouhou.pojo.dto.user.UserUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.user.UserMapper;
import com.huangjun.shouhou.pojo.po.user.UserPO;
import com.huangjun.shouhou.pojo.qo.user.UserQO;
import com.huangjun.shouhou.pojo.vo.user.UserListVO;
import com.huangjun.shouhou.pojo.vo.user.UserShowVO;
import com.huangjun.shouhou.service.user.UserService;
import com.huangjun.shouhou.web.AbstractController;
import com.huangjun.shouhou.web.api.user.UserAPI;
import com.huangjun.shouhou.web.constant.WebConst;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * 【用户表(员工表)】控制器
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@RestController
@RequestMapping(WebConst.ModulePath.USER + "/user")
public class UserController extends AbstractController implements UserAPI {

    @Autowired
    private UserService userService;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserShowVO> save(@Valid @RequestBody UserAddDTO userAddDTO) throws Exception {
        UserPO user = userService.save(userAddDTO);
        return ResponseEntity.created(new URI(WebConst.ModulePath.USER + "/user/" + user.getUserId()))
                .body(UserMapper.INSTANCE.toShowVO(user));
    }

    @Override
    @PutMapping
    public ResponseEntity<UserShowVO> update(@Valid @RequestBody UserUpdateDTO userUpdateDTO) {
        UserPO user = userService.update(userUpdateDTO);
        return ResponseEntity.ok(UserMapper.INSTANCE.toShowVO(user));
    }

    @Override
    @GetMapping
    public ResponseEntity<PageVO<UserListVO>> list(@Valid UserQO userQO) {
        PageVO<UserListVO> page = userService.list(userQO);
        return ResponseEntity.ok(page);
    }

    @Override
    @GetMapping(value = "/options")
    public ResponseEntity<List<OptionVO<Long, String>>> findOptions(OptionQO<Long, String> qo) {
        List<OptionVO<Long, String>> options = userService.findOptions(qo);
        return ResponseEntity.ok(options);
    }

    @Override
    @GetMapping(value = "/{userId}")
    public ResponseEntity<UserShowVO> show(@PathVariable Long userId) {
        UserShowVO userShowVO = userService.show(userId);
        return ResponseEntity.ok(userShowVO);
    }

    @Override
    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<Integer> delete(@PathVariable Long userId) {
        int count = userService.delete(userId);
        return ResponseEntity.ok(count);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<Integer> deleteBatch(@RequestBody Long[] id) {
        if (ArrayUtils.isEmpty(id)) {
            throw new BusinessException(ErrorCode.PARAM_IS_NULL);
        }
        int count = userService.delete(id);
        return ResponseEntity.ok(count);
    }

}


