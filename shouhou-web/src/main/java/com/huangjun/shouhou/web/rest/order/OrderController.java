package com.huangjun.shouhou.web.rest.order;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.common.util.DateUtil;
import com.huangjun.shouhou.constant.OrderStatus;
import com.huangjun.shouhou.constant.ServerType;
import com.huangjun.shouhou.excel.handler.ConstConstraintHandler;
import com.huangjun.shouhou.excel.handler.TemplateCellStyleStrategy;
import com.huangjun.shouhou.excel.handler.TitleDescriptionWriteHandler;
import com.huangjun.shouhou.excel.listener.SyncReadExcelListener;
import com.huangjun.shouhou.pojo.dto.order.OrderAddDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderExcelDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.order.OrderMapper;
import com.huangjun.shouhou.pojo.po.order.OrderPO;
import com.huangjun.shouhou.pojo.qo.order.OrderQO;
import com.huangjun.shouhou.pojo.vo.order.OrderExcelVO;
import com.huangjun.shouhou.pojo.vo.order.OrderListVO;
import com.huangjun.shouhou.pojo.vo.order.OrderShowVO;
import com.huangjun.shouhou.service.order.OrderService;
import com.huangjun.shouhou.web.AbstractController;
import com.huangjun.shouhou.web.api.order.OrderAPI;
import com.huangjun.shouhou.web.constant.WebConst;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 【工单中心】控制器
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@RestController
@RequestMapping(WebConst.ModulePath.ORDER + "/order")
public class OrderController extends AbstractController implements OrderAPI {

    @Autowired
    private OrderService orderService;
    @Autowired
    private Validator validator;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<OrderShowVO> save(@Valid @RequestBody OrderAddDTO orderAddDTO) throws Exception {
        OrderPO order = orderService.save(orderAddDTO);
        return ResponseEntity.created(new URI(WebConst.ModulePath.ORDER + "/order/" + order.getOrderId()))
                .body(OrderMapper.INSTANCE.toShowVO(order));
    }

    @Override
    @PutMapping
    public ResponseEntity<OrderShowVO> update(@Valid @RequestBody OrderUpdateDTO orderUpdateDTO) {
        OrderPO order = orderService.update(orderUpdateDTO);
        return ResponseEntity.ok(OrderMapper.INSTANCE.toShowVO(order));
    }

    @Override
    @GetMapping
    public ResponseEntity<PageVO<OrderListVO>> list(@Valid OrderQO orderQO) {
        PageVO<OrderListVO> page = orderService.list(orderQO);
        return ResponseEntity.ok(page);
    }

    @Override
    @GetMapping(value = "/{orderId}")
    public ResponseEntity<OrderShowVO> show(@PathVariable Long orderId) {
        OrderShowVO orderShowVO = orderService.show(orderId);
        return ResponseEntity.ok(orderShowVO);
    }

    @Override
    @DeleteMapping(value = "/{orderId}")
    public ResponseEntity<Integer> delete(@PathVariable Long orderId) {
        int count = orderService.delete(orderId);
        return ResponseEntity.ok(count);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<Integer> deleteBatch(@RequestBody Long[] id) {
        if (ArrayUtils.isEmpty(id)) {
            throw new BusinessException(ErrorCode.PARAM_IS_NULL);
        }
        int count = orderService.delete(id);
        return ResponseEntity.ok(count);
    }

    @Override
    @GetMapping("/export")
    public void exportExcel(@Valid OrderQO orderQO, HttpServletResponse response) throws Exception {
        orderQO.setPageSize(Integer.MAX_VALUE);
        orderQO.setPageNo(1);
        List<OrderListVO> list = orderService.list(orderQO).getList();
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("工单中心导出", "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), OrderExcelVO.class)
                .sheet()
                .doWrite(OrderMapper.INSTANCE.toExcelVOList(list));
    }

    @Override
    @PostMapping("/import")
    public ResponseEntity<Integer> importExcel(@RequestPart MultipartFile file) throws Exception {
        SyncReadExcelListener<OrderExcelDTO> excelListener = new SyncReadExcelListener();
        EasyExcel.read(file.getInputStream())
                .head(OrderExcelDTO.class)
                .sheet()
                .headRowNumber(3)
                .registerReadListener(excelListener)
                .doRead();
        List<OrderAddDTO> list = excelListener.getList().stream()
                .map(excelDTO -> {
                    OrderAddDTO addDTO = OrderMapper.INSTANCE.fromExcelDTO(excelDTO);
                    // 校验数据
                    Set<ConstraintViolation<OrderAddDTO>> set = validator.validate(addDTO);
                    if (!set.isEmpty()) {
                        ConstraintViolation<OrderAddDTO> violation = set.stream().findFirst().get();
                        String errorMsg = "第" + (excelDTO.getRowIndex() + 1) + "行数据不合法：" + violation.getMessage();
                        throw new ConstraintViolationException(errorMsg, set);
                    }
                    return addDTO;
                })
                .collect(Collectors.toList());
        int count = orderService.batchSave(list);
        return ResponseEntity.ok(count);
    }

    @Override
    @GetMapping("/template")
    public void downloadExcelTemplate(HttpServletResponse response) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String title = "工单中心导入模板(" + DateUtil.getDateStr(new Date()) + ")";
        String fileName = URLEncoder.encode(title, "utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        String[] description = new String[]{
                "模版前三行标题请勿修改",
                "带“*”号为必填项",
                "“客户”、“客户产品”、“选择维修工”请填入id值",
        };
        String[] serverTypeConstraint = Arrays.stream(ServerType.values()).map(ServerType::getDesc).toArray(String[]::new);
        String[] orderStatusConstraint = Arrays.stream(OrderStatus.values()).map(OrderStatus::getDesc).toArray(String[]::new);
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream())
                .registerWriteHandler(new ConstConstraintHandler(serverTypeConstraint, 3, 3, 5, 5))
                .registerWriteHandler(new ConstConstraintHandler(orderStatusConstraint, 3, 3, 9, 9))
                // 第一行是标题，第二行是说明
                .registerWriteHandler(new TitleDescriptionWriteHandler(title, description, OrderExcelDTO.class))
                // 自定义模板单元格样式
                .registerWriteHandler(new TemplateCellStyleStrategy())
                .build();
        WriteSheet writeSheet = EasyExcel.writerSheet(0, "Sheet1")
                .head(OrderExcelDTO.class)
                // 从第三行开始写表头
                .relativeHeadRowIndex(2)
                .build();
        excelWriter.write(Arrays.asList(OrderExcelDTO.example()), writeSheet);

        excelWriter.finish();
    }

}


