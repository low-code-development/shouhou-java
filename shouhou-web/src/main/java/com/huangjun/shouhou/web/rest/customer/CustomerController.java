package com.huangjun.shouhou.web.rest.customer;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerAddDTO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.customer.CustomerMapper;
import com.huangjun.shouhou.pojo.po.customer.CustomerPO;
import com.huangjun.shouhou.pojo.qo.customer.CustomerQO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerListVO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerShowVO;
import com.huangjun.shouhou.service.customer.CustomerService;
import com.huangjun.shouhou.web.AbstractController;
import com.huangjun.shouhou.web.api.customer.CustomerAPI;
import com.huangjun.shouhou.web.constant.WebConst;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * 【客户管理】控制器
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@RestController
@RequestMapping(WebConst.ModulePath.CUSTOMER + "/customer")
public class CustomerController extends AbstractController implements CustomerAPI {

    @Autowired
    private CustomerService customerService;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CustomerShowVO> save(@Valid @RequestBody CustomerAddDTO customerAddDTO) throws Exception {
        CustomerPO customer = customerService.save(customerAddDTO);
        return ResponseEntity.created(new URI(WebConst.ModulePath.CUSTOMER + "/customer/" + customer.getCustomerId()))
                .body(CustomerMapper.INSTANCE.toShowVO(customer));
    }

    @Override
    @PutMapping
    public ResponseEntity<CustomerShowVO> update(@Valid @RequestBody CustomerUpdateDTO customerUpdateDTO) {
        CustomerPO customer = customerService.update(customerUpdateDTO);
        return ResponseEntity.ok(CustomerMapper.INSTANCE.toShowVO(customer));
    }

    @Override
    @GetMapping
    public ResponseEntity<PageVO<CustomerListVO>> list(@Valid CustomerQO customerQO) {
        PageVO<CustomerListVO> page = customerService.list(customerQO);
        return ResponseEntity.ok(page);
    }

    @Override
    @GetMapping(value = "/options")
    public ResponseEntity<List<OptionVO<Long, String>>> findOptions(OptionQO<Long, String> qo) {
        List<OptionVO<Long, String>> options = customerService.findOptions(qo);
        return ResponseEntity.ok(options);
    }

    @Override
    @GetMapping(value = "/{customerId}")
    public ResponseEntity<CustomerShowVO> show(@PathVariable Long customerId) {
        CustomerShowVO customerShowVO = customerService.show(customerId);
        return ResponseEntity.ok(customerShowVO);
    }

    @Override
    @DeleteMapping(value = "/{customerId}")
    public ResponseEntity<Integer> delete(@PathVariable Long customerId) {
        int count = customerService.delete(customerId);
        return ResponseEntity.ok(count);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<Integer> deleteBatch(@RequestBody Long[] id) {
        if (ArrayUtils.isEmpty(id)) {
            throw new BusinessException(ErrorCode.PARAM_IS_NULL);
        }
        int count = customerService.delete(id);
        return ResponseEntity.ok(count);
    }

}


