DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
    `customer_id` bigint(20) AUTO_INCREMENT COMMENT '主键ID',
    `name` varchar(50) DEFAULT NULL COMMENT '客户名称',
    `number` varchar(50) DEFAULT NULL COMMENT '客户编号',
    `phone_name` varchar(32) DEFAULT NULL COMMENT '联系人',
    `phone` varchar(32) DEFAULT NULL COMMENT '联系电话',
    `address` varchar(100) DEFAULT NULL COMMENT '地址',
    `owner` varchar(32) DEFAULT NULL COMMENT '客户负责人',
    `created_time` datetime NOT NULL COMMENT '创建时间【yyyy-MM-dd HH:mm:ss】',
    `created_by` varchar(20) NOT NULL COMMENT '创建人【最大长度20】',
    `operated_time` datetime NOT NULL COMMENT '修改时间【yyyy-MM-dd HH:mm:ss】',
    PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客户管理';

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
    `product_id` bigint(20) AUTO_INCREMENT COMMENT '主键ID',
    `name` varchar(50) DEFAULT NULL COMMENT '产品名称',
    `number` varchar(50) DEFAULT NULL COMMENT '产品编号',
    `type` int(11) DEFAULT NULL COMMENT '产品分类：具体详细请见 枚举管理查看',
    `created_time` datetime NOT NULL COMMENT '创建时间【yyyy-MM-dd HH:mm:ss】',
    `created_by` varchar(20) NOT NULL COMMENT '创建人【最大长度20】',
    `operated_time` datetime NOT NULL COMMENT '修改时间【yyyy-MM-dd HH:mm:ss】',
    PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品管理';

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
    `user_id` bigint(20) AUTO_INCREMENT COMMENT '主键ID',
    `name` varchar(32) NOT NULL COMMENT '姓名',
    `username` varchar(50) NOT NULL COMMENT '账号',
    `password` varchar(100) NOT NULL COMMENT '密码',
    `phone` varchar(32) DEFAULT NULL COMMENT '电话',
    `role` int(11) DEFAULT NULL COMMENT '角色',
    `created_time` datetime NOT NULL COMMENT '创建时间【yyyy-MM-dd HH:mm:ss】',
    `created_by` varchar(20) NOT NULL COMMENT '创建人【最大长度20】',
    `operated_time` datetime NOT NULL COMMENT '修改时间【yyyy-MM-dd HH:mm:ss】',
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表(员工表)';

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
    `order_id` bigint(20) AUTO_INCREMENT COMMENT '主键ID',
    `repair_number` varchar(100) DEFAULT NULL COMMENT '关联报修单号',
    `order_number` varchar(50) DEFAULT NULL COMMENT '工单编号',
    `customer_contacts` varchar(32) DEFAULT NULL COMMENT '客户联系人',
    `phone` varchar(32) DEFAULT NULL COMMENT '电话',
    `address` varchar(100) DEFAULT NULL COMMENT '地址',
    `server_type` int(11) NOT NULL COMMENT '服务类型',
    `money` decimal(20,2) DEFAULT NULL COMMENT '本次服务费',
    `remark` varchar(200) DEFAULT NULL COMMENT '备注',
    `file_path` varchar(100) DEFAULT NULL COMMENT '上传附件',
    `status` int(11) DEFAULT NULL COMMENT '工单状态',
    `customer_id` bigint(20) DEFAULT NULL COMMENT '客户',
    `product_id` bigint(20) DEFAULT NULL COMMENT '客户产品',
    `user_id` bigint(20) DEFAULT NULL COMMENT '选择维修工',
    `created_time` datetime NOT NULL COMMENT '创建时间【yyyy-MM-dd HH:mm:ss】',
    `operated_time` datetime NOT NULL COMMENT '修改时间【yyyy-MM-dd HH:mm:ss】',
    PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='工单中心';

DROP TABLE IF EXISTS `repair`;

CREATE TABLE `repair` (
    `repair_id` bigint(20) AUTO_INCREMENT COMMENT '主键ID',
    `repair_number` varchar(50) DEFAULT NULL COMMENT '报修单编号',
    `customer_contacts` varchar(32) DEFAULT NULL COMMENT '客户联系人',
    `phone` varchar(32) DEFAULT NULL COMMENT '电话',
    `address` varchar(100) DEFAULT NULL COMMENT '地址',
    `file_path` varchar(100) DEFAULT NULL COMMENT '上传附件',
    `repair_type` int(11) NOT NULL COMMENT '报修类型',
    `remark` varchar(200) DEFAULT NULL COMMENT '备注',
    `status` int(11) DEFAULT NULL COMMENT '工单状态',
    `customer_id` bigint(20) DEFAULT NULL COMMENT '客户',
    `product_id` bigint(20) DEFAULT NULL COMMENT '客户产品',
    `user_id` bigint(20) DEFAULT NULL COMMENT '选择维修工',
    `created_time` datetime NOT NULL COMMENT '创建时间【yyyy-MM-dd HH:mm:ss】',
    `created_by` varchar(20) DEFAULT NULL COMMENT '创建人【最大长度20】',
    `operated_time` datetime NOT NULL COMMENT '修改时间【yyyy-MM-dd HH:mm:ss】',
    PRIMARY KEY (`repair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='报修中心';

