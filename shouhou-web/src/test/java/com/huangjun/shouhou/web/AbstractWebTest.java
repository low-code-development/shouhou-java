package com.huangjun.shouhou.web;

import com.huangjun.shouhou.AbstractTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

/**
 * web单元测试抽象类
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@AutoConfigureMockMvc(printOnlyOnFailure = false)
public abstract class AbstractWebTest extends AbstractTest {

    @Autowired
    protected MockMvc restMockMvc;

}

