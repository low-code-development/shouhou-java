package com.huangjun.shouhou.web.rest.customer;

import com.google.common.collect.Lists;
import com.huangjun.common.util.JsonUtil;
import com.huangjun.shouhou.help.customer.CustomerHelper;
import com.huangjun.shouhou.pojo.dto.customer.CustomerAddDTO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerUpdateDTO;
import com.huangjun.shouhou.pojo.po.customer.CustomerPO;
import com.huangjun.shouhou.web.AbstractWebTest;
import com.huangjun.shouhou.web.constant.WebConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 【客户管理】单元测试
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class CustomerControllerTest extends AbstractWebTest {

    @Autowired
    private CustomerHelper customerHelper;


    /**
     * 新增【客户管理】
     */
    @Test
    public void save() throws Exception {
        CustomerAddDTO addDTO = customerHelper.getCustomerAddDTO();
        restMockMvc.perform(post(WebConst.ModulePath.CUSTOMER + "/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(addDTO)))
                .andExpect(status().isCreated());
    }

    /**
     * 修改【客户管理】
     */
    @Test
    public void update() throws Exception {
        CustomerPO customer = customerHelper.saveCustomerExample();
        CustomerUpdateDTO updateDTO = customerHelper.getCustomerUpdateDTO(customer);
        restMockMvc.perform(put(WebConst.ModulePath.CUSTOMER + "/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(updateDTO)))
                .andExpect(status().isOk());
    }

    /**
     * 分页查询【客户管理】
     */
    @Test
    public void list() throws Exception {
        CustomerPO customer = customerHelper.saveCustomerExample();
        restMockMvc.perform(get(WebConst.ModulePath.CUSTOMER + "/customer"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.list.length()").value(is(1)));
    }

    /**
     * 查询【客户管理】选项列表
     */
    @Test
    public void findOptions() throws Exception {
        CustomerPO customer = customerHelper.saveCustomerExample();
        restMockMvc.perform(get(WebConst.ModulePath.CUSTOMER + "/customer/options"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(is(1)));
    }

    /**
     * 查看【客户管理】详情
     */
    @Test
    public void show() throws Exception {
        CustomerPO customer = customerHelper.saveCustomerExample();
        restMockMvc.perform(get(WebConst.ModulePath.CUSTOMER + "/customer/{customerId}", customer.getCustomerId()))
                .andExpect(status().isOk());
    }

    /**
     * 删除单个【客户管理】
     */
    @Test
    public void del() throws Exception {
        CustomerPO customer = customerHelper.saveCustomerExample();
        restMockMvc.perform(delete(WebConst.ModulePath.CUSTOMER + "/customer/{customerId}", customer.getCustomerId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 批量删除【客户管理】
     */
    @Test
    public void deleteBatch() throws Exception {
        CustomerPO customer = customerHelper.saveCustomerExample();
        restMockMvc.perform(delete(WebConst.ModulePath.CUSTOMER + "/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(Lists.newArrayList(customer.getCustomerId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }


}
