package com.huangjun.shouhou.web.rest.repair;

import com.google.common.collect.Lists;
import com.huangjun.common.util.JsonUtil;
import com.huangjun.shouhou.help.repair.RepairHelper;
import com.huangjun.shouhou.pojo.dto.repair.RepairAddDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairUpdateDTO;
import com.huangjun.shouhou.pojo.po.repair.RepairPO;
import com.huangjun.shouhou.web.AbstractWebTest;
import com.huangjun.shouhou.web.constant.WebConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 【报修中心】单元测试
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class RepairControllerTest extends AbstractWebTest {

    @Autowired
    private RepairHelper repairHelper;


    /**
     * 新增【报修中心】
     */
    @Test
    public void save() throws Exception {
        RepairAddDTO addDTO = repairHelper.getRepairAddDTO(null, null, null);
        restMockMvc.perform(post(WebConst.ModulePath.REPAIR + "/repair")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(addDTO)))
                .andExpect(status().isCreated());
    }

    /**
     * 修改【报修中心】
     */
    @Test
    public void update() throws Exception {
        RepairPO repair = repairHelper.saveRepairExample(null, null, null);
        RepairUpdateDTO updateDTO = repairHelper.getRepairUpdateDTO(repair);
        restMockMvc.perform(put(WebConst.ModulePath.REPAIR + "/repair")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(updateDTO)))
                .andExpect(status().isOk());
    }

    /**
     * 分页查询【报修中心】
     */
    @Test
    public void list() throws Exception {
        RepairPO repair = repairHelper.saveRepairExample(null, null, null);
        restMockMvc.perform(get(WebConst.ModulePath.REPAIR + "/repair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.list.length()").value(is(1)));
    }

    /**
     * 查看【报修中心】详情
     */
    @Test
    public void show() throws Exception {
        RepairPO repair = repairHelper.saveRepairExample(null, null, null);
        restMockMvc.perform(get(WebConst.ModulePath.REPAIR + "/repair/{repairId}", repair.getRepairId()))
                .andExpect(status().isOk());
    }

    /**
     * 删除单个【报修中心】
     */
    @Test
    public void del() throws Exception {
        RepairPO repair = repairHelper.saveRepairExample(null, null, null);
        restMockMvc.perform(delete(WebConst.ModulePath.REPAIR + "/repair/{repairId}", repair.getRepairId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 批量删除【报修中心】
     */
    @Test
    public void deleteBatch() throws Exception {
        RepairPO repair = repairHelper.saveRepairExample(null, null, null);
        restMockMvc.perform(delete(WebConst.ModulePath.REPAIR + "/repair")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(Lists.newArrayList(repair.getRepairId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 导入【报修中心】excel
     */
    @Test
    public void importExcel() throws Exception {
        // 首先下载excel模板
        MvcResult mvcResult = restMockMvc.perform(get(WebConst.ModulePath.REPAIR + "/repair/template"))
                .andExpect(status().isOk())
                .andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        // 将模板原封不动导入
        MockMultipartFile file = new MockMultipartFile("file", response.getContentAsByteArray());
        restMockMvc.perform(multipart(WebConst.ModulePath.REPAIR + "/repair/import")
                .file(file))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }


}
