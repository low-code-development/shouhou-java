package com.huangjun.shouhou.help.user;

import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.pojo.dto.user.*;
import com.huangjun.shouhou.pojo.po.user.*;
import com.huangjun.shouhou.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.huangjun.shouhou.pojo.example.user.UserExample.*;

@Component
public class UserHelper {

    @Autowired
    private UserService userService;

    /**
     * 生成add测试数据
     *
     * @return
     */
    public UserAddDTO getUserAddDTO() {
        UserAddDTO dto = new UserAddDTO();
        dto.setName(E_NAME);
        dto.setUsername(E_USERNAME);
        dto.setPassword(E_PASSWORD);
        dto.setPhone(E_PHONE);
        dto.setRole(SafeUtil.getInteger(E_ROLE));
        return dto;
    }


    /**
     * 生成update测试数据
     *
     * @return
     */
    public UserUpdateDTO getUserUpdateDTO(UserPO user) {
        UserUpdateDTO dto = new UserUpdateDTO();
        dto.setUserId(user.getUserId());
        dto.setName(user.getName());
        dto.setUsername(user.getUsername());
        dto.setPassword(user.getPassword());
        dto.setPhone(user.getPhone());
        dto.setRole(user.getRole());
        return dto;
    }

    /**
     * 保存示例
     *
     * @return
     */
    public UserPO saveUserExample() {
        UserAddDTO addDTO = this.getUserAddDTO();
        return userService.save(addDTO);
    }


}

