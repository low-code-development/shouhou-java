package com.huangjun.shouhou.help.customer;

import com.huangjun.shouhou.pojo.dto.customer.*;
import com.huangjun.shouhou.pojo.po.customer.*;
import com.huangjun.shouhou.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.huangjun.shouhou.pojo.example.customer.CustomerExample.*;

@Component
public class CustomerHelper {

    @Autowired
    private CustomerService customerService;

    /**
     * 生成add测试数据
     *
     * @return
     */
    public CustomerAddDTO getCustomerAddDTO() {
        CustomerAddDTO dto = new CustomerAddDTO();
        dto.setName(E_NAME);
        dto.setNumber(E_NUMBER);
        dto.setPhoneName(E_PHONE_NAME);
        dto.setPhone(E_PHONE);
        dto.setAddress(E_ADDRESS);
        dto.setOwner(E_OWNER);
        return dto;
    }


    /**
     * 生成update测试数据
     *
     * @return
     */
    public CustomerUpdateDTO getCustomerUpdateDTO(CustomerPO customer) {
        CustomerUpdateDTO dto = new CustomerUpdateDTO();
        dto.setCustomerId(customer.getCustomerId());
        dto.setName(customer.getName());
        dto.setNumber(customer.getNumber());
        dto.setPhoneName(customer.getPhoneName());
        dto.setPhone(customer.getPhone());
        dto.setAddress(customer.getAddress());
        dto.setOwner(customer.getOwner());
        return dto;
    }

    /**
     * 保存示例
     *
     * @return
     */
    public CustomerPO saveCustomerExample() {
        CustomerAddDTO addDTO = this.getCustomerAddDTO();
        return customerService.save(addDTO);
    }


}

