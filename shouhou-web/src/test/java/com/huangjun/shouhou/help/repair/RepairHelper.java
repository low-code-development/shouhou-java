package com.huangjun.shouhou.help.repair;

import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.pojo.dto.repair.*;
import com.huangjun.shouhou.pojo.po.repair.*;
import com.huangjun.shouhou.service.repair.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.huangjun.shouhou.pojo.example.repair.RepairExample.*;

@Component
public class RepairHelper {

    @Autowired
    private RepairService repairService;

    /**
     * 生成add测试数据
     *
     * @return
     */
    public RepairAddDTO getRepairAddDTO(Long customerId, Long productId, Long userId) {
        RepairAddDTO dto = new RepairAddDTO();
        dto.setRepairNumber(E_REPAIR_NUMBER);
        dto.setCustomerContacts(E_CUSTOMER_CONTACTS);
        dto.setPhone(E_PHONE);
        dto.setAddress(E_ADDRESS);
        dto.setFilePath(E_FILE_PATH);
        dto.setRepairType(SafeUtil.getInteger(E_REPAIR_TYPE));
        dto.setRemark(E_REMARK);
        dto.setStatus(SafeUtil.getInteger(E_STATUS));
        dto.setCustomerId(customerId);
        dto.setProductId(productId);
        dto.setUserId(userId);
        return dto;
    }


    /**
     * 生成update测试数据
     *
     * @return
     */
    public RepairUpdateDTO getRepairUpdateDTO(RepairPO repair) {
        RepairUpdateDTO dto = new RepairUpdateDTO();
        dto.setRepairId(repair.getRepairId());
        dto.setRepairNumber(repair.getRepairNumber());
        dto.setCustomerContacts(repair.getCustomerContacts());
        dto.setPhone(repair.getPhone());
        dto.setAddress(repair.getAddress());
        dto.setFilePath(repair.getFilePath());
        dto.setRepairType(repair.getRepairType());
        dto.setRemark(repair.getRemark());
        dto.setStatus(repair.getStatus());
        dto.setCustomerId(repair.getCustomerId());
        dto.setProductId(repair.getProductId());
        dto.setUserId(repair.getUserId());
        return dto;
    }

    /**
     * 保存示例
     *
     * @return
     */
    public RepairPO saveRepairExample(Long customerId, Long productId, Long userId) {
        RepairAddDTO addDTO = this.getRepairAddDTO(customerId, productId, userId);
        return repairService.save(addDTO);
    }


}

