package com.huangjun.shouhou.help.product;

import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.pojo.dto.product.*;
import com.huangjun.shouhou.pojo.po.product.*;
import com.huangjun.shouhou.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.huangjun.shouhou.pojo.example.product.ProductExample.*;

@Component
public class ProductHelper {

    @Autowired
    private ProductService productService;

    /**
     * 生成add测试数据
     *
     * @return
     */
    public ProductAddDTO getProductAddDTO() {
        ProductAddDTO dto = new ProductAddDTO();
        dto.setName(E_NAME);
        dto.setNumber(E_NUMBER);
        dto.setType(SafeUtil.getInteger(E_TYPE));
        return dto;
    }


    /**
     * 生成update测试数据
     *
     * @return
     */
    public ProductUpdateDTO getProductUpdateDTO(ProductPO product) {
        ProductUpdateDTO dto = new ProductUpdateDTO();
        dto.setProductId(product.getProductId());
        dto.setName(product.getName());
        dto.setNumber(product.getNumber());
        dto.setType(product.getType());
        return dto;
    }

    /**
     * 保存示例
     *
     * @return
     */
    public ProductPO saveProductExample() {
        ProductAddDTO addDTO = this.getProductAddDTO();
        return productService.save(addDTO);
    }


}

