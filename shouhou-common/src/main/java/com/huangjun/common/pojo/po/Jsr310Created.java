package com.huangjun.common.pojo.po;


/**
 * 创建人&创建时间-jsr310时间API
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Jsr310Created extends CreatedBy, Jsr310CreatedTime {
}

