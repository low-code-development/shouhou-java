package com.huangjun.common.pojo.po;


/**
 * 是否逻辑删除接口
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Deleted {

    Boolean getDeleted();

    void setDeleted(Boolean deleted);

}

