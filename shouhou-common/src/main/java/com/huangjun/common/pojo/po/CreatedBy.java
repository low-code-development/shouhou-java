package com.huangjun.common.pojo.po;


/**
 * 创建人接口
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface CreatedBy {

    String getCreatedBy();

    void setCreatedBy(String createdBy);

}

