package com.huangjun.common.pojo.po;


/**
 * 操作人接口
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface OperatedBy {

    String getOperatedBy();

    void setOperatedBy(String operatedBy);

}

