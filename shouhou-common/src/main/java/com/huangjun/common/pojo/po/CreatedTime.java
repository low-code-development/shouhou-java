package com.huangjun.common.pojo.po;

import java.util.Date;

/**
 * 创建时间接口
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface CreatedTime {

    Date getCreatedTime();

    void setCreatedTime(Date createdTime);

}

