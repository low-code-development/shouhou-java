package com.huangjun.common.pojo.po;

import java.util.Date;

/**
 * 操作时间接口
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface OperatedTime {

    Date getOperatedTime();

    void setOperatedTime(Date operatedTime);
}

