package com.huangjun.common.pojo.vo;

import com.huangjun.common.util.JsonUtil;

import java.io.Serializable;

/**
 * 抽象VO
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public abstract class AbstractVO implements Serializable {

    private static final long serialVersionUID = -1417748095004687576L;

    @Override
    public String toString() {
        return JsonUtil.toJSONString(this);
    }

}

