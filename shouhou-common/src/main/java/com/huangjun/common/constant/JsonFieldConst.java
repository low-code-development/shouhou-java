package com.huangjun.common.constant;


/**
 * json常量
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class JsonFieldConst {

    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_DATETIME_FORMAT_2 = "yyyy-MM-dd HH:mm:ss.SSS";

}

