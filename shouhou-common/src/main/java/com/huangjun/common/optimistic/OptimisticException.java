package com.huangjun.common.optimistic;


/**
 * 乐观锁异常
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class OptimisticException extends RuntimeException {

    public OptimisticException() {

    }

    public OptimisticException(String message) {
        super(message);
    }
}

