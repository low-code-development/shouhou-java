package com.huangjun.shouhou.dao.order;

import com.huangjun.common.dao.DAO;
import com.huangjun.shouhou.pojo.po.order.OrderPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 【工单中心】数据库操作
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Repository
@Mapper
public interface OrderDAO extends DAO<OrderPO> {

    int getCountByCustomerId(Long customerId);

    int getCountByProductId(Long productId);

    int getCountByUserId(Long userId);


}



