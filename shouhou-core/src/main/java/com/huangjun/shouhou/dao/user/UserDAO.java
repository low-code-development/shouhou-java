package com.huangjun.shouhou.dao.user;

import com.huangjun.common.dao.DAO;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.shouhou.pojo.po.user.UserPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 【用户表(员工表)】数据库操作
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Repository
@Mapper
public interface UserDAO extends DAO<UserPO> {

    List<OptionVO<Long, String>> findOptions(OptionQO<Long, String> qo);


}



