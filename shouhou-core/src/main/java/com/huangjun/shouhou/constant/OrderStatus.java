package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【工单状态】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum OrderStatus {

    /**
     * 待接受
     */
    DAIJIESHOU(1, "待接受"),
    /**
     * 待维修
     */
    DAIWEIXIU(2, "待维修"),
    /**
     * 已完成
     */
    YIWANCHENG(3, "已完成"),
    /**
     * 已关闭
     */
    YIGUANBI(4, "已关闭"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2,3,4";

    private static final Map<Integer, OrderStatus> LOOKUP = new HashMap<>();

    static {
        for (OrderStatus e : OrderStatus.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    OrderStatus(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static OrderStatus find(Integer value) {
        return LOOKUP.get(value);
    }

    public static OrderStatus findByDesc(String desc) {
        for (OrderStatus e : OrderStatus.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        OrderStatus theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        OrderStatus theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        OrderStatus theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

