package com.huangjun.shouhou.pojo.po.order;

import com.huangjun.common.pojo.po.AbstractPO;
import com.huangjun.common.pojo.po.Jsr310CreatedTime;
import com.huangjun.common.pojo.po.Jsr310OperatedTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 工单中心
 * <p>工单中心
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class OrderPO extends AbstractPO implements Jsr310CreatedTime, Jsr310OperatedTime {

    /**
     * 主键ID
     */
    private Long orderId;

    /**
     * 关联报修单号
     */
    private String repairNumber;

    /**
     * 工单编号
     */
    private String orderNumber;

    /**
     * 客户联系人
     */
    private String customerContacts;

    /**
     * 电话
     */
    private String phone;

    /**
     * 地址
     */
    private String address;

    /**
     * 服务类型
     *
     * @see com.huangjun.shouhou.constant.ServerType
     */
    private Integer serverType;

    /**
     * 本次服务费
     */
    private BigDecimal money;

    /**
     * 备注
     */
    private String remark;

    /**
     * 上传附件
     */
    private String filePath;

    /**
     * 工单状态
     *
     * @see com.huangjun.shouhou.constant.OrderStatus
     */
    private Integer status;

    /**
     * 客户
     */
    private Long customerId;

    /**
     * 客户产品
     */
    private Long productId;

    /**
     * 选择维修工
     */
    private Long userId;

    /**
     * 创建时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime createdTime;

    /**
     * 修改时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime operatedTime;


}

