package com.huangjun.shouhou.pojo.dto.repair;

import com.huangjun.common.pojo.dto.AbstractDTO;
import com.huangjun.common.validator.Const;
import com.huangjun.shouhou.constant.RepairStatus;
import com.huangjun.shouhou.constant.RepairType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static com.huangjun.shouhou.pojo.example.repair.RepairExample.*;

/**
 * 修改【报修中心】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "修改【报修中心】的参数")
public class RepairUpdateDTO extends AbstractDTO {

    @ApiModelProperty(notes = N_REPAIR_ID, example = E_REPAIR_ID, required = true)
    @NotNull
    private Long repairId;

    @ApiModelProperty(notes = N_REPAIR_NUMBER, example = E_REPAIR_NUMBER)
    @Length(max = 50)
    private String repairNumber;

    @ApiModelProperty(notes = N_CUSTOMER_CONTACTS, example = E_CUSTOMER_CONTACTS)
    @Length(max = 32)
    private String customerContacts;

    @ApiModelProperty(notes = N_PHONE, example = E_PHONE)
    @Length(max = 32)
    private String phone;

    @ApiModelProperty(notes = N_ADDRESS, example = E_ADDRESS)
    @Length(max = 100)
    private String address;

    @ApiModelProperty(notes = N_FILE_PATH, example = E_FILE_PATH)
    @Length(max = 100)
    private String filePath;

    @ApiModelProperty(notes = N_REPAIR_TYPE, example = E_REPAIR_TYPE, required = true, allowableValues = RepairType.VALUES_STR)
    @NotNull
    @Const(constClass = RepairType.class)
    private Integer repairType;

    @ApiModelProperty(notes = N_REMARK, example = E_REMARK)
    @Length(max = 200)
    private String remark;

    @ApiModelProperty(notes = N_STATUS, example = E_STATUS, allowableValues = RepairStatus.VALUES_STR)
    @Const(constClass = RepairStatus.class)
    private Integer status;

    @ApiModelProperty(notes = N_CUSTOMER_ID, example = E_CUSTOMER_ID)
    private Long customerId;

    @ApiModelProperty(notes = N_PRODUCT_ID, example = E_PRODUCT_ID)
    private Long productId;

    @ApiModelProperty(notes = N_USER_ID, example = E_USER_ID)
    private Long userId;



}

