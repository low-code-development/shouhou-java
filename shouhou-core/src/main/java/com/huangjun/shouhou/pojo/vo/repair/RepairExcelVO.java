package com.huangjun.shouhou.pojo.vo.repair;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.huangjun.common.pojo.vo.AbstractVO;
import com.huangjun.shouhou.excel.converter.LocalDateTimeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 【报修中心】excel导出对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class RepairExcelVO extends AbstractVO {

    @ExcelProperty("主键")
    @ColumnWidth(12)
    private Long repairId;

    @ExcelProperty("报修单编号")
    @ColumnWidth(15)
    private String repairNumber;

    @ExcelProperty("报修类型")
    @ColumnWidth(15)
    private String repairType;

    @ExcelProperty("工单状态")
    @ColumnWidth(15)
    private String status;

    @ExcelProperty("客户")
    @ColumnWidth(15)
    private Long customerId;

    @ExcelProperty("客户产品")
    @ColumnWidth(15)
    private Long productId;

    @ExcelProperty(value = "创建时间", converter = LocalDateTimeConverter.class)
    @ColumnWidth(25)
    private LocalDateTime createdTime;

    @ExcelProperty(value = "修改时间", converter = LocalDateTimeConverter.class)
    @ColumnWidth(25)
    private LocalDateTime operatedTime;

    @ExcelProperty("客户名称")
    @ColumnWidth(15)
    private String customerName;

    @ExcelProperty("产品名称")
    @ColumnWidth(15)
    private String productName;

    @ExcelProperty("姓名")
    @ColumnWidth(15)
    private String userName;



}

