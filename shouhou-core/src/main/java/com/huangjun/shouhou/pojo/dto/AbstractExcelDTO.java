package com.huangjun.shouhou.pojo.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.huangjun.common.pojo.dto.AbstractDTO;
import lombok.Data;

/**
 * 抽象excel数据传输对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
public abstract class AbstractExcelDTO extends AbstractDTO {

    /**
     * 行号
     */
    @ExcelIgnore
    private Integer rowIndex;


}

