package com.huangjun.shouhou.pojo.po.user;

import com.huangjun.common.pojo.po.AbstractPO;
import com.huangjun.common.pojo.po.Jsr310Created;
import com.huangjun.common.pojo.po.Jsr310OperatedTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 用户表(员工表)
 * <p>用户表(员工表)
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class UserPO extends AbstractPO implements Jsr310Created, Jsr310OperatedTime {

    /**
     * 主键ID
     */
    private Long userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 电话
     */
    private String phone;

    /**
     * 角色
     *
     * @see com.huangjun.shouhou.constant.Role
     */
    private Integer role;

    /**
     * 创建时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime createdTime;

    /**
     * 创建人【最大长度20】
     */
    private String createdBy;

    /**
     * 修改时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime operatedTime;


}

