package com.huangjun.shouhou.pojo.mapper.repair;

import com.huangjun.shouhou.pojo.dto.repair.RepairAddDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairExcelDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairUpdateDTO;
import com.huangjun.shouhou.pojo.po.repair.RepairPO;
import com.huangjun.shouhou.pojo.vo.repair.RepairExcelVO;
import com.huangjun.shouhou.pojo.vo.repair.RepairListVO;
import com.huangjun.shouhou.pojo.vo.repair.RepairShowVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 【报修中心】映射
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Mapper
public interface RepairMapper {

    RepairMapper INSTANCE = Mappers.getMapper(RepairMapper.class);

    /**
     * addDTO映射po
     *
     * @param repairAddDTO
     * @return
     */
    RepairPO fromAddDTO(RepairAddDTO repairAddDTO);

    /**
     * 将updateDTO中的值设置到po
     *
     * @param repairPO
     * @param repairUpdateDTO
     */
    void setUpdateDTO(@MappingTarget RepairPO repairPO, RepairUpdateDTO repairUpdateDTO);

    /**
     * po映射showVO
     *
     * @param repairPO
     * @return
     */
    RepairShowVO toShowVO(RepairPO repairPO);


    /**
     * excelDTO映射addDTO
     *
     * @param dto
     * @return
     */
    @Mappings({
            @Mapping(target = "repairType", expression = "java(com.huangjun.shouhou.constant.RepairType.descToValue(dto.getRepairType()))"),
            @Mapping(target = "status", expression = "java(com.huangjun.shouhou.constant.RepairStatus.descToValue(dto.getStatus()))"),
    })
    RepairAddDTO fromExcelDTO(RepairExcelDTO dto);

    /**
     * listVO列表转excelVO列表
     *
     * @param list
     * @return
     */
    List<RepairExcelVO> toExcelVOList(List<RepairListVO> list);

    /**
     * listVO转excelVO
     *
     * @param vo
     * @return
     */
    @Mappings({
            @Mapping(target = "repairType", expression = "java(com.huangjun.shouhou.constant.RepairType.valueToDesc(vo.getRepairType()))"),
            @Mapping(target = "status", expression = "java(com.huangjun.shouhou.constant.RepairStatus.valueToDesc(vo.getStatus()))"),
    })
    RepairExcelVO toExcelVO(RepairListVO vo);


}

