package com.huangjun.shouhou.pojo.qo.customer;

import com.huangjun.common.pojo.qo.PageQO;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import static com.huangjun.shouhou.pojo.example.customer.CustomerExample.*;

/**
 * 查询【客户管理】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class CustomerQO extends PageQO {

    @ApiParam(value = N_NAME, example = E_NAME)
    @Length(max = 50, message = "name最大长度不能超过{max}")
    private String name;

    @ApiParam(value = N_NUMBER, example = E_NUMBER)
    @Length(max = 50, message = "number最大长度不能超过{max}")
    private String number;

    @ApiParam(value = N_OWNER, example = E_OWNER)
    @Length(max = 32, message = "owner最大长度不能超过{max}")
    private String owner;

    @ApiParam(value = "创建时间排序标识【1升序,-1降序,0不排序】", example = "1")
    private Integer createdTimeSortSign;

    @ApiParam(value = "修改时间排序标识【1升序,-1降序,0不排序】", example = "1")
    private Integer operatedTimeSortSign;


}

