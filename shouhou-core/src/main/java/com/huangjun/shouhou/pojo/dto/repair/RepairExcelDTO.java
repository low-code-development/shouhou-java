package com.huangjun.shouhou.pojo.dto.repair;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.constant.RepairStatus;
import com.huangjun.shouhou.constant.RepairType;
import com.huangjun.shouhou.pojo.dto.AbstractExcelDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import static com.huangjun.shouhou.pojo.example.repair.RepairExample.*;

/**
 * excel导入【报修中心】的数据传输对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class RepairExcelDTO extends AbstractExcelDTO {

    @ExcelProperty("报修单编号")
    @ColumnWidth(15)
    private String repairNumber;

    @ExcelProperty("客户联系人")
    @ColumnWidth(15)
    private String customerContacts;

    @ExcelProperty("电话")
    @ColumnWidth(15)
    private String phone;

    @ExcelProperty("地址")
    @ColumnWidth(15)
    private String address;

    @ExcelProperty("上传附件")
    @ColumnWidth(15)
    private String filePath;

    @ExcelProperty("报修类型*")
    @ColumnWidth(15)
    private String repairType;

    @ExcelProperty("备注")
    @ColumnWidth(15)
    private String remark;

    @ExcelProperty("工单状态")
    @ColumnWidth(15)
    private String status;

    @ExcelProperty("客户")
    @ColumnWidth(15)
    private Long customerId;

    @ExcelProperty("客户产品")
    @ColumnWidth(15)
    private Long productId;

    @ExcelProperty("选择维修工")
    @ColumnWidth(15)
    private Long userId;


    /**
     * 创建模板示例
     *
     * @return
     */
    public static RepairExcelDTO example() {
        RepairExcelDTO example = new RepairExcelDTO();
        example.setRepairNumber(E_REPAIR_NUMBER);
        example.setCustomerContacts(E_CUSTOMER_CONTACTS);
        example.setPhone(E_PHONE);
        example.setAddress(E_ADDRESS);
        example.setFilePath(E_FILE_PATH);
        example.setRepairType(RepairType.valueToDesc(SafeUtil.getInteger(E_REPAIR_TYPE)));
        example.setRemark(E_REMARK);
        example.setStatus(RepairStatus.valueToDesc(SafeUtil.getInteger(E_STATUS)));
        example.setCustomerId(SafeUtil.getLong(E_CUSTOMER_ID));
        example.setProductId(SafeUtil.getLong(E_PRODUCT_ID));
        example.setUserId(SafeUtil.getLong(E_USER_ID));
        return example;
    }

}


